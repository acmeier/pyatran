# -*- coding: utf-8 -*-

"""pyatran.input
================

Handling of SCIATRAN input files:

- extraction of input file information from runtime configurations
- copying input files to new directories
- adapting runtime configurations to reflect new directory structure

.. autosummary::
   :toctree: api/

   save_input_files

"""

from .input import save_input_files
