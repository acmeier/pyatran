numpy>=1.7
pandas>=0.15
xarray>=0.9

setuptools>=27.3

pytest-flake8
pytest-cov
pytest-runner
pytest
pycodestyle>=2.4.0
mccabe<0.6.0,>=0.5.0
