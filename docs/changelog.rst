**********
What's New
**********

.. contents::
   :depth: 1
   :local:


Unreleased
==========

Version 0.3.1, 2018-10-08
=========================

:DOC: Starting with v0.3.1, pyatran is now `citeable with a DOI
      <https://doi.org/10.5281/zenodo.1439269>`__, provided via
      `Zenodo <https://zenodo.org/>`__ (see `#49
      <https://gitlab.com/andreas-h/pyatran/issues/49>`__).

:ENH: :py:func:`pyatran.output.read_sciatran.read_sce_absorber` now
      also reads the integrated vertical absorber profiles, i.e., the
      vertical column densities, from the ``SCE_ABSORBER.OUT`` file
      (see `#47 <https://gitlab.com/andreas-h/pyatran/issues/47>`__).


Version 0.3.0, 2018-09-03
=========================

Backwards incompatible changes
------------------------------

:ENH: :py:func:`pyatran.runtime.setup_runtime` now has two kwargs ``clean_log``
      and ``clean_output`` to handle potentially existing log and output files
      from previous runs in the same runtime directory separately.  The previous
      kwarg ``clean`` has been removed.  The default behavior is to clean both
      log and output files.

:ENH: As the concept of *layers* does not really exist in SCIATRAN,
      :py:func:`pyatran.output.read_sciatran_output` now does not
      assign *layers* to the results of ``block_amf`` simulations,
      i.e., the ``block_amf`` DataArray does not have the
      ``altitude_bnds`` attribute any more (see `#11
      <https://gitlab.com/andreas-h/pyatran/issues/11>`__).

:MAINT: The module :py:mod:`pyatran.plot` has been removed (see `#44
      <https://gitlab.com/andreas-h/pyatran/issues/44>`__).

New features
------------

:ENH: :py:func:`pyatran.input.save_input_files` now supports the
      ``Home directory`` keyword from ``control.inp`` (see `#39
      <https://gitlab.com/andreas-h/pyatran/issues/39>`__).

:ENH: :py:func:`pyatran.output.read_sciatran_output` now supports reading
      SCIATRAN 3.0 output (see `#33
      <https://gitlab.com/andreas-h/pyatran/issues/33>`__).

:ENH: :py:func:`pyatran.runtime.setup_runtime` now checks for the presence of an
      ``errors.log`` file and rotates it "out of the way" if present.  This is
      done with the new function :py:func:`pyatran.runtime.runtime.rotate_log`
      (see `#30 <https://gitlab.com/andreas-h/pyatran/issues/30>`__).


Bug fixes
---------

:BUG: :py:class:`pyatran.output.Result` didn't issue a ``DeprecationWarning`` as
       it was supposed to (see `#34
       <https://gitlab.com/andreas-h/pyatran/issues/34>`__).

:BUG: :py:func:`pyatran.input.input._save_input_files_controlprofinp` didn't
      allow an empty list for parameter ``Trace gas replacement profiles`` (see
      `#28 <https://gitlab.com/andreas-h/pyatran/issues/28>`__).

:DOC: The docstring of :py:func:`pyatran.runtime.setup_runtime` gave a wrong
      default value for the *clean* kwarg (see `#27
      <https://gitlab.com/andreas-h/pyatran/issues/27>`__).


Version 0.2.0, 2017-09-13
=========================

New features
------------

:ENH: new function :py:func:`pyatran.output.read_inp_par` to read
      ``SCE_INP-PARS.OUT`` files

:ENH: new function :py:func:`pyatran.output.read_xsection_data` to read
      cross-section data from files linked in ``SCE_INP_PARS.OUT``

:ENH: function :py:func:`pyatran.output.read_sciatran_output` now adds comments
      to some output variables which are affected by known SCIATRAN bugs /
      limitations (see `#11 <https://gitlab.com/andreas-h/pyatran/issues/11>`__)


Bug fixes
---------

:BUG: fix wrong lower level boundary for surface_height > 0
   (fixes `#24 <https://gitlab.com/andreas-h/pyatran/issues/24>`__)

:BUG: fix reading DOM_V output
   (fixes `#17 <https://gitlab.com/andreas-h/pyatran/issues/17>`__)


Version 0.1.10, 2017-09-11
==========================

.. note::

   Version v0.1.10 is a maintenance release.  Its main purpose is to check if
   the automatic release process is ready for version v0.2.0.

:ENH: pyatran now supports the parameter file ``control_rrs.inp``.
:ENH: Runtime creation will now put relative paths into the SCIATRAN control
      files (which will still all be copied to ``DATA_IN/`` in the process)
      instead of absolute paths.  This makes the runtime directories
      self-contained and transferable.
:ENH: There is a new function :py:func:`pyatran.output.read_sciatran_output` to
      read SCIATRAN output into an ``xarray.Dataset``.

Version 0.1.9
=============

Version v0.1.9 was never really released; the tag had to be introduced for
created for technical reasons.

Version 0.1.8, 2016-09-23
=========================

:BUG: :py:func:`pyatran.input.save_input_files` didn't check for existing input
      data directory.

:BUG: :py:func:`pyatran.util.mkbasedir` didn't work under Python 2.


Version 0.1.7, 2016-09-13
=========================

:ENH: :py:class:`pyatran.output.Result` is now a sub-class of ``dict``

:BUG: :py:class:`pyatran.output.Result` can now handle ``output_map.inf`` files
      where the azimuth angle is defined *@ Observer position*.

:BUG: :py:class:`pyatran.output.Result` fixed ``output_map.inf`` problems with
      reading Stokes Component

Version 0.1.6, 2016-02-17
=========================

:BUG: Missing function to create directories

Version 0.1.5, 2016-02-17
=========================

:DOC: Quite a few documentation improvements

Version 0.1.4, 2015-11-23
=========================

:ENH: :py:class:`pyatran.runtime.Runtime` now supports sym-linking the
      SCIATRAN executable instead of copying it

:ENH: :py:class:`pyatran.params` now supports setting control_brdf.inp

:ENH: :py:class:`pyatran.output.Result` now works under Python3

:ENH: :py:class:`pyatran.params` has now support for manual aerosol
      settings

:ENH: :py:class:`pyatran.output.Result` now supports reading
      bamf_int.dat

:ENH: :py:class:`pyatran.params` has now support for WMO aerosol
      parameterization

:ENH: :py:class:`pyatran.output.Result` now supports reading output
      from CDI mode simulations

:ENH: :py:class:`pyatran.output.Result` now supports reading local
      optical parameters

Version 0.1.3, 2015-04-29
=========================

:ENH: :py:class:`pyatran.output.Result` now properly supports reading
      irradiances

:ENH: :py:class:`pyatran.output.Result` now supports reading Raman mode results

Version 0.1.2, 2014-11-05
=========================

:ENH: :py:class:`pyatran.output.Result` now supports reading VOD mode results

:ENH: Don't create None members of :py:class:`pyatran.output.Result` for
      missing output files.

:BUG: :py:class:`pyatran.output.Result` didn't read intensity correctly.

:BUG: :py:func:`pyatran.runtime.store_results` excluded
      ``sciaconf.json`` when ``save_executable=False``.

:ENH: :py:func:`pyatran.params.load_json` added to read SCIATRAN
      parameters from JSON into an OrderedDict

Version 0.1.1, 2014-10-23
=========================

:BUG: :py:class:`pyatran.output.Result` didn't handle all kinds
      of *output_map* correctly.

Version 0.1.0, 2014-10-22
=========================

This is the first proper release of pyatran.
