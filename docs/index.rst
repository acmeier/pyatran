.. pyatran documentation master file, created by
   sphinx-quickstart on Wed Oct 15 14:05:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pyatran -- handling SCIATRAN from Python
========================================

pyatran is a collection of Python methods and classes to access the `SCIATRAN
radiative transfer model <http://www.iup.uni-bremen.de/sciatran/>`__ from the
`Python <https://www.python.org>`__ programming language.


Contact
-------

In case of any questions, please contact the maintainer `Andreas Hilboll
<hilboll@uni-bremen.de>`__.


Reporting bugs
--------------

For reporting bugs, it would be most convenient if you could use the `issue
tracker <https://gitlab.com/andreas-h/pyatran/issues>`__ on Gitlab.  In case you
don't have an account on Gitlab, you can also `send an e-mail
<mailto:incoming+andreas-h/pyatran@gitlab.com>`__ to create an issue.


Citing pyatran
--------------

When you use pyatran for your research, please be so kind and cite
it. We provide a `Zenodo citation and DOI
<https://doi.org/10.5281/zenodo.1439269>`_ for this purpose:

.. image:: https://zenodo.org/badge/doi/10.5281/zenodo.1439269.svg
   :target: https://doi.org/10.5281/zenodo.1439269

An example BibTeX entry::

   @misc{pyatran_v0_3_1,
         author = {Hilboll, Andreas and
	           Sanders, Abram F. J. and
	           Borchardt, Jakob},
         title  = {pyatran: v0.3.1},
         month  = oct,
         year   = 2018,
         doi    = {10.5281/zenodo.1439269},
         url    = {https://doi.org/10.5281/zenodo.1439269}
        }


Contents
--------

.. toctree::
   :maxdepth: 2

   changelog
   install
   params
   api
   dev


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

